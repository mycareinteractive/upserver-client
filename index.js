var Upserver = require('./lib/upserver.core').default;
var UpserverEvents = require('./lib/upserver.signalr').UpserverEvents;

var upserver = new Upserver();
upserver.events = UpserverEvents;

window.upserver = upserver;
module.exports = upserver;