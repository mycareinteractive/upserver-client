var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.resolve(__dirname, 'dist');
var APP_DIR = path.resolve(__dirname, '');

var config = {
    debug: true,
    devtool: 'source-map',

    context: APP_DIR,

    entry: [
        './index.js'
    ],

    output: {
        filename: 'upserver.min.js',
        path: BUILD_DIR,
    },

    module: {
        loaders: [
            {test: /\.js(x)?$/, exclude: /node_modules/, loaders: ['babel']}
        ]
    }
};

module.exports = config;