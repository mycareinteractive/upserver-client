﻿import $ from 'jquery';
window.$ = window.jQuery = $; // make jQuery global so signalR doesn't choke
require('ms-signalr-client');

/**
 * @module upserver-client
 */

/**
 * @global
 * @description window.upserver is a singleton instance of {@link Upserver}.  You should always use this object
 */
window.upserver;

/**
 * @ignore
 * Exposed signalR object of Upserver.
 * SignalR object should never be created manually.  Instead use window.upserver.signalR
 */
class SignalR {

    constructor() {

        /**
         * Whether real-time features are enabled or not
         * @type {boolean}
         */
        this.enabled = false;
        this.reconnectTimer = undefined;
    }

    init(upserver) {
        this.enabled = false;
        var deferred = $.Deferred();

        var self = this;
        console.log('We are trying to enable signalR.');
        // Include server Hub script
        $.getScript(upserver.options.host + '/signalr/hubs')
            .done(function () {
                //wire up SignalR
                self.addListners($.connection);

                console.log('HUBS ARE DONE');

                //start hub
                $.connection.hub.url = upserver.options.host + '/signalr/';
                $.connection.hub.qs = {"access_token": upserver.accessToken};
                $.connection.hub.logging = true;

                console.log('START HUB REQUESTS');

                // disconnection event
                $.connection.hub.disconnected(function () {
                    console.log('SignalR disconnected, will retry in 120 seconds');
                    self.reconnectTimer = setTimeout(function () {
                        self.connect(upserver.options);
                    }, 120000); // Restart connection after 2 minutes.
                });

                // reconnecting event
                $.connection.hub.reconnecting(function() {
                    console.log('SignalR reconnecting');
                });

                // reconnect event
                $.connection.hub.reconnected(function () {
                    console.log('SignalR reconnected');
                });

                // start signalR
                self.connect(upserver.options, deferred);

                console.log('HUB REQUESTS HAVE BEEN MADE');

            })
            .fail(function () {
                deferred.reject('Error! Failed to get ' + upserver.options.host + '/signalr/hubs');
            });

        return deferred.promise();
    }

    connect(options, deferred) {
        var self = this;

        clearTimeout(self.reconnectTimer);

        $.connection.hub.start({
            withCredentials: false,
            transport: options.transport
        }).done(function () {
            self.enabled = true;
            console.log('SignalR connected, connection ID=' + $.connection.hub.id + ', transport=' + $.connection.hub.transport.name);

            if (deferred)
                deferred.resolve();

        }).fail(function () {
            console.log('Unable to connect to signalR.');

            self.reconnectTimer = setTimeout(function () {
                self.connect(options);
            }, 120000); // Restart connection after 2 minutes.

            if (deferred)
                deferred.reject('Error! Failed start SignalR hub.');
        });
    }

    addListners(connection) {
        // Internal Hubs implementations
        var patientHub = connection.patientHub;
        var deviceHub = connection.deviceHub;

        const events = UpserverEvents;

        // heartbeat
        // setInterval(function() {
        //     deviceHub.server.heartbeat();
        // }, 60000);

        // device
        deviceHub.client.locationUpdated = function () {
            $(upserver).triggerHandler(events.device.locationUpdated);
        };

        deviceHub.client.patientAssociated = function () {
            $(upserver).triggerHandler(events.device.patientAssociated);
        };

        deviceHub.client.patientDissociated = function () {
            $(upserver).triggerHandler(events.device.patientDissociated);
        };

        deviceHub.client.refresh = function () {
            $(upserver).triggerHandler(events.device.refresh);
        };

        deviceHub.client.reboot = function () {
            $(upserver).triggerHandler(events.device.reboot);
        };

        deviceHub.client.triggerRemote = function (keyName) {
            $(upserver).triggerHandler(events.device.triggerRemote, keyName);
        };

        deviceHub.client.triggerTap = function (elementId) {
            $(upserver).triggerHandler(events.device.triggerTap, elementId);
        };

        deviceHub.client.triggerScript = function (script) {
            $(upserver).triggerHandler(events.device.triggerScript, script);
        };

        deviceHub.client.hijackStart = function (param) {
            $(upserver).triggerHandler(events.device.hijackStart, param);
        };

        deviceHub.client.hijackStop = function (param) {
            $(upserver).triggerHandler(events.device.hijackStop, param);
        };

        deviceHub.client.hijackStatus = function (param) {
            $(upserver).triggerHandler(events.device.hijackStatus, param);
        };

        // patient
        patientHub.client.visitChanged = function () {
            $(upserver).triggerHandler(events.patient.visitChanged);
        };

        patientHub.client.profileChanged = function () {
            $(upserver).triggerHandler(events.patient.profileChanged);
        };

        patientHub.client.clinicalRecordChanged = function () {
            $(upserver).triggerHandler(events.patient.clinicalRecordChanged);
        };

        patientHub.client.userRecordChanged = function () {
            $(upserver).triggerHandler(events.patient.userRecordChanged);
        };

        patientHub.client.preferenceChanged = function () {
            $(upserver).triggerHandler(events.patient.preferenceChanged);
        };
    }
}

// UpServer events
// -------------

/**
 * @memberOf Upserver
 */
const UpserverEvents = {
    device: {
        /**
         * Fired when device location has changed
         * @event locationUpdated
         */
        locationUpdated: 'locationUpdated',
        /**
         * Fired when patient admitted or transferred in
         * @event patientAssociated
         */
        patientAssociated: 'patientAssociated',
        /**
         * Fired when patient discharged or transferred out
         * @event patientDissociated
         */
        patientDissociated: 'patientDissociated',
        /**
         * Fired when device needs refresh
         * @event refresh
         */
        refresh: 'refresh',
        /**
         * Fired when device needs reboot
         * @event reboot
         */
        reboot: 'reboot',
        /**
         * Fired when device needs to trigger a remote button
         * @event triggerRemote
         */
        triggerRemote: 'triggerRemote',
        /**
         * Fired when device needs to trigger a tap event
         * @event triggerTap
         */
        triggerTap: 'triggerTap',
        /**
         * Fired when server sends script
         * @event triggerScript
         */
        triggerScript: 'triggerScript',
        /**
         * Fired when eHospital Hijack start
         * @event hijackStart
         */
        hijackStart: 'hijackStart',
        /**
         * Fired when eHospital Hijack stop
         * @event hijackStop
         */
        hijackStop: 'hijackStop',
        /**
         * Fired when eHospital Hijack status check
         * @event hijackStatus
         */
        hijackStatus: 'hijackStatus'
    },
    patient: {
        /**
         * Fired when patient visit status changed
         * @event visitChanged
         */
        visitChanged: 'visitChanged',
        /**
         * Fired when patient profile changed
         * @event profileChanged
         */
        profileChanged: 'profileChanged',
        /**
         * Fired when patient clinical records changed
         * @event recordChanged
         */
        clinicalRecordChanged: 'clinicalRecordChanged',
        /**
         * Fired when patient user records changed
         * @event userRecordChanged
         */
        userRecordChanged: 'userRecordChanged',
        /**
         * Fired when patient preference changed
         * @event preferenceChanged
         */
        preferenceChanged: 'preferenceChanged'
    }
};


export {UpserverEvents};
export default SignalR;