﻿import $ from 'jquery';
import SignalR from './upserver.signalr';

window.console.info = window.console.info || window.console.log;
window.console.warn = window.console.warn || window.console.log;
window.console.error = window.console.error || window.console.log;

/**
 * JavaScript client library to connect to UpServer backend.
 *
 * @module upserver-client
 */

// Default options
const defaultOptions = {
    host: '',
    clientId: '',
    deviceId: '',
    grantType: 'client_credentials',
    scope: 'device patient',
    disableSignalR: false
};

// update query parameter
function updateUrlParameter(uri, key, value) {
    // remove the hash part before operating on the uri
    var i = uri.indexOf('#');
    var hash = i === -1 ? ''  : uri.substr(i);
    uri = i === -1 ? uri : uri.substr(0, i);

    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        uri = uri.replace(re, '$1' + key + "=" + value + '$2');
    } else {
        uri = uri + separator + key + "=" + value;
    }
    return uri + hash;  // finally append the hash as well
}

/**
 * An Upserver is a singleton so there is no need to "new" it.  window.upserver is already exposed for you.
 * Just call [upserver.init()]{@link Upserver#init} to initialize it.
 * @alias module:upserver-client
 */
class Upserver {

    constructor() {
        /**
         * API access token
         */
        this.accessToken = '';

        /**
         * Active options
         */
        this.options = {};

        /**
         * @member
         * @type {SignalR} SignalR object
         */
        this.signalR = new SignalR();

        this.baseUrl = '/api/v1';

        this.authHeader = '';

        this.locale = 'en';
    }

    /**
     * @function
     * @name init
     * Initialize the upserver object.
     * @param {Object} options initialize options.
     * @param options.host {String} The API server host, eg: "" for localhost or "http://192.168.1.23" for remote host (cross origin is supported).
     * @param options.clientId {String} The portal/app client unique ID.
     * @param options.deviceId {String} Device ID for "client_credentials" grant.
     * @param [options.grantType] {String} Grant type, by default "client_credentials".
     * @param [options.scope] {String} Grant scope, by default "device patient".
     * @param [options.disableSignalR] {Boolean} Disable server push feature, by default "false".
     * @param [options.transport] {String} SignalR transport method.  Can be string or array. Eg: ['webSockets', 'longPolling'].
     * @returns {Promise} It returns a jQuery Promise so you can chain <code>done()</code> and <code>fail()</code> to it.
     */
    init(options) {
        var self = this;
        $.extend(self.options, defaultOptions, options);
        self.baseUrl = self.options.host + '/api/v1';

        var deferred = $.Deferred();

        self._auth()
            .done(function () {

                if (!options.disableSignalR) {
                    // init signalR
                    console.log('SingnalR is ACTIVATED: Intializing signalR...');
                    self.signalR.init(self).always(function () {
                        console.log('## We have initialized SignalR ##');
                        deferred.resolve();
                    });
                } else {
                    console.log('SignalR is disabled. No server push capability will be available.');
                    deferred.resolve();
                }
            })
            .fail(function (jqXHR, status) {
                console.log('Initialization failed. Error: ' + status);
                deferred.reject(jqXHR, status);
            });

        return deferred.promise();
    }

    /**
     * @function api
     * A shorthand function for public API call.
     * @param {String} url API endpoint URL.  Prefix is not required. eg: "/token", "/contents".
     * @param {String} [method] "GET", "POST", "PUT" or "DELETE".  Default "GET".
     * @param {Object} [data] Data parameter to send to API.
     * @returns {Promise} It returns a jQuery Promise so you can chain <code>done()</code> and <code>fail()</code> to it.
     */
    api(url, method, data) {
        var self = this;
        if (method == 'DELETE' && data) {
            // RFC requires delete parameters in URL not in form
            $.each(data, function (k, v) {
                url = updateUrlParameter(url, k, v);
            });
        }
        return $.ajax({
            url: self.baseUrl + url,
            method: method || 'GET',
            headers: {"Authorization": self.authHeader, "Accept-Language": self.locale},
            data: data,
            cache: false
        });
    }

    /**
     * @function on
     * Attach upserver event handlers.
     * @param {String} events Events. See [all the events here]{@link event:locationUpdated}
     * @param {Anything} [data] Data to be passed to the handler
     * @param {Function} [handler] A function to execute when the event is triggered. The value false is also allowed as a shorthand for a function that simply does return false.
     * @returns {Object} It returns upserver instance itself so chaining multiple <code>on().on()</code> is possible.
     * @example
     * // reload when patientAssociated event happens
     * window.upserver.on('patientAssociated', function() {
         *     console.log('reloading due to patient admission.');
         *     location.reload(true);
         * });
     */
    on(events, data, handler) {
        var self = this;
        $(self).on.apply($(self), arguments);
        return this;
    }

    /**
     * @function trigger
     * Trigger upserver event handlers
     * @param {String} events events Events.
     * @returns {Upserver}
     */
    trigger(event) {
        var args = Array.prototype.slice.call(arguments);
        $(this).trigger.apply($(this), args);
        return this;
    }

    /**
     * @function setLanguage
     * Set API language.
     * @param langtag 'en' or 'es' or other language
     * @remarks If set to empty string or undefined, clears cookie and reset to default language
     */
    setLanguage(langtag) {
        if (!langtag) {
            document.cookie = "i18n.langtag=" + langtag + ";path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT";
        }
        else {
            document.cookie = "i18n.langtag=" + langtag + ";path=/;expires=0";
        }
        return this;
    }

    // Private methods
    // -------------

    // Authenticate the API and get access token.
    _auth() {
        var self = this;
        var data = {
            "async": true,
            "url": self.baseUrl + "/token",
            "method": "POST",
            "data": {
                "client_id": self.options.clientId,
                "grant_type": self.options.grantType,
                "scope": self.options.scope,
                "device_id": self.options.deviceId
            }
        };

        // Our own deferred object
        var deferred = $.Deferred();

        $.ajax(data).done(function (response) {
            if (response['access_token']) {
                self.accessToken = response['access_token'];
                self.authHeader = 'Bearer ' + self.accessToken;
                console.log('token: ' + self.accessToken);
                deferred.resolve(response);
                console.log('We have completed the authorization routine successfully.');
            }
            else {
                var err = 'token not found';
                console.log(err);
                deferred.reject(null, err);
            }
        }).fail(function (jqXHR, status) {
            var err = 'auth error: ' + status;
            console.log(err);
            deferred.reject(jqXHR, err);
        });

        return deferred.promise();
    }

}

export default Upserver;

